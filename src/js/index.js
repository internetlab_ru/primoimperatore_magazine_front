import * as $ from 'jquery';
import {
   initMaskedInput,
   initPlaceholders
} from './components/form';
import {
   InitLoginForm
} from "./components/init-login-form";
import {
   InitRestoreForm
} from "./components/init-restore-form";
import {
   Catalogue
} from './components/catalogue';
import {
   ModalWindow
} from './components/modal-window';
import {Product} from './components/product';
import {Cart} from './components/cart';
import {initCustomSelects} from './components/selects';
import {Profile} from './components/Profile';


$(function () {
   initScripts();
});

function initScripts() {

   updateViewportHeight();

   $(window).on('resize', () => {
      updateViewportHeight();
   });

   function isDesktop() {
      return (window.innerWidth > 1000);
   }

   //селекты хедера
   initCustomSelects();

   //формирование селекта языка в зависимости от передаваемого значения с бека
   let languageDropdown = $('[data-language-dropdown]').data('settings');

   let langDropDownHTML = '';
   languageDropdown.forEach( lang =>{
      if(lang.selected == 'true'){
         if(isDesktop()){
            $('#laguage_select').text(`${lang.text}`);
         }else{
            $('#laguage_select_mobile').find('span').text(`${lang.text}`)
         }

      }else{
         langDropDownHTML += `<span><a href="${lang.value}">${lang.text}</a></span>`
      }
      
   })

   $('.languages').html(langDropDownHTML);

   //модальные окна
   new ModalWindow();

   //инициализация логин формы
   new InitLoginForm();

   //инициализация формы восст пароля
   new InitRestoreForm();

   //каталог
   new Catalogue();

   //item + slider
   new Product();

   //корзина
   new Cart();

   //профиль
   new Profile();

    // Инициализация плейсхолдеров и масок
    initMaskedInput();
    initPlaceholders();


   //открытие модальной галереи в мобилке
   if(!isDesktop()){
      $('.item-block__content-gallery-item').on('click',e=>{
         $('body').css('overflow', 'hidden');
      })
   }

   //закрытие модальной галереи -> оверфлоу в авто

   $('#close_gallery i').on('click', e => {
      $('body').css('overflow', 'auto')
   })

   //При закрытии формы, все свичи выключаются, бади оверфлоу в авто

   $('#close_choose_modal').on('click', e => {
      $('body').css('overflow', 'auto');
      /* clearChooseForm(); */
   })

   /* $('#close_choose_modal_second').on('click', e => {
      if($('#close_choose_modal').hasClass('filled')){
         return;
      }
      clearChooseForm();
   }) */

   $(document).on("change keyup input click", "input[type='tel']", function() {
      if(this.value.match(/[^0-9]/g)){
          this.value = this.value.replace(/[^0-9]/g, "");
      };
  });

}

const updateViewportHeight = () => {
   if ($(document).width() < 1000) {
      document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 64}px`);
   } else {
      document.documentElement.style.setProperty('--viewport-height', `${window.innerHeight - 96}px`);
   }
};

/* const clearChooseForm = () => {
   $('.choose-block-wrapper').find('label.switcher-gray').each((index,item)=>{
      const parentBlock = $(item).parents('.choose-block-wrapper');
      $(item).removeClass('active');
      parentBlock.removeClass('open');
      parentBlock.find('input.input-count').val('');
      parentBlock.find('.choose-cost').text('Стоимость: 0 €');
      parentBlock.find('span.label').css('display', 'inline-block');
      parentBlock.find('input.input-count').css('border','1px solid #ececec');
   })
} */