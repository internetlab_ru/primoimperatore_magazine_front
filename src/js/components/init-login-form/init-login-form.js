import * as $ from 'jquery';
import { initFormWithValidate } from "../form";
import {showLoader, hideLoader} from "../loader";
import { translate } from '../helper/helpers';

export default class InitLoginForm{
   constructor(){
      this.$form = $('#login_form');
      this.$formMessageBlock = this.$form.find('.form-message');
      this.$showPass = $('#show_pass');

      this.userId = 0;
      
      if (!this.$form.length) return false;
      
      this.successForm = this.successForm.bind(this);
      this.errorForm = this.errorForm.bind(this);
      this.clearForm = this.clearForm.bind(this);

      this.init();
   }

   init(){
      initFormWithValidate(this.$form);

      this.$showPass.on('click', (e)=>{
         e.preventDefault();
         $('#show_pass').toggleClass('icon-eye');

         if($('#login_password').attr('type') === 'password'){
            $('#login_password').attr('type','text');
         }else if($('#login_password').attr('type') === 'text'){
            $('#login_password').attr('type','password');
         }
      })

      this.submit(this.successForm, this.errorForm);
   }

   submit(successFn, errorFn) {
      this.$form.on('submit',  (e)=> {
         e.preventDefault();

         showLoader($('#login_form'),$('#login_loader'));

         let $formData = {};

         $(this).find('input, textarea, select').each(function () {
            $formData[this.name] = $(this).val();
         });

         let data = {
            login: $formData.login_form_name,
            password: $formData.login_form_password
         };

         data=$('#login_form').serializeArray();

         $.ajax({
            url: $('#login_form').data('request'),
            type: 'POST',
            dataType: 'text',
            data: data,
            success:  (res) => {
               successFn(res);
            },
            error:  (res) => {
               errorFn(res);
            },
            timeout: 30000
         });
      })
   }

   successForm(res) {
      this.clearForm();
      
      window.location.href = `https://order.primoimperatore.com/${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `${$('#lang').val()}`}`;
       // window.location.href = `https://primomarket.internetlab.ru/${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `${$('#lang').val()}`}`;
   }

   errorForm(res) {
     const trueResponse = JSON.parse(res.responseText.split('\n')[1]);
     
      if(trueResponse.success === true) {
         this.userId = trueResponse.userId;
         this.successForm();
         return false;
      }

      /* res для выведения ошибки в message-title и message-subtitle*/
      this.clearForm();

      hideLoader($('#login_loader'));
      $('.login-wrapper').addClass('error');

      $('.form-message').removeClass('hide');
      $('.form-message').find('.message-title').text(translate('Ошибка'));
      $('.form-message').find('.message-subtitle').text(translate('Попробуйте еще раз'));

      $('.form-message').find('#close_error').on('click', function(){
         $('.form-message').addClass('hide');
         $('.login-wrapper').removeClass('error');
         $('#login_form').removeClass('hide');
      })
   }

   clearForm() {
      this.$form[0].reset();
      this.$form.find('.field').removeClass('success').addClass('empty');
   }
}