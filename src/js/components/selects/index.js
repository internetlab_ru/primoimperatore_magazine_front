import * as $  from 'jquery';

let initCustomSelects = () => {

   const isDesktop = () => {
      return (window.innerWidth > 1000);
   }

   const showCustomSelect = (param) => {
   $('.languages').removeClass('hide');
   if (param === 'desktop') {
      $('.header-ui__language').find('.icon-arrow-down-header')
         .css('transform', 'rotate(180deg)');

   } else if (param === 'mobile') {
      $('.header-mobile-content__language-block').find('.icon-arrow-down-header')
         .css('transform', 'rotate(180deg)');
   }
   }

   const hideCustomSelect = (param) => {
   
      $('.languages').addClass('hide');
      if (param === 'desktop') {
         $('.header-ui__language').find('.icon-arrow-down-header')
            .css('transform', 'rotate(0)');
   
      } else if (param === 'mobile') {
         $('.header-mobile-content__language-block').find('.icon-arrow-down-header')
            .css('transform', 'rotate(0)');
      }
   }

   $('.languages').on('click', 'span', (e) => {
      const $this = e.currentTarget;
      $('#laguage_select').html(`${$(e.currentTarget).html()}`);
      $('#laguage_select').attr('value', `${$(e.currentTarget).attr('value')}`);
      hideCustomSelect('desktop');
   })

   if (!isDesktop()) {
      $('#laguage_select_mobile').on('click', () => {
         if ($('.languages').hasClass('hide')) {
            showCustomSelect('mobile');
         } else {
            hideCustomSelect('mobile');
         }
      })

      $('.languages').on('click', 'span', (e) => {
         const $this = e.currentTarget;
         $('#laguage_select_mobile').html(`${$(e.currentTarget).html()}`);
         $('#laguage_select_mobile').attr('value', `${$(e.currentTarget).attr('value')}`);
         hideCustomSelect('mobile');
      })
   }

   //select для профиля десктоп
   $('#header_email').on('click', () => {
      if ($('.user-select').hasClass('hide')) {
         $('.user-select').removeClass('hide');
      } else {
         $('.user-select').addClass('hide');
      }

      $('.user-select').on('mouseleave', () => {
         $('.user-select').addClass('hide');
      })
   })

   //select для профиля мобильная
   if (!isDesktop()) {
      $('#header_profile_icon').on('click', () => {
         if ($('.user-select').hasClass('hide')) {
            $('.user-select').removeClass('hide');
         } else {
            $('.user-select').addClass('hide');
         }
      })
   }

    //select для языков
   $('.header-ui__language').on('click', () => {
      if ($('.languages').hasClass('hide')) {
         showCustomSelect('desktop');
      } else {
         hideCustomSelect('desktop');
      }

      $('.languages').on('mouseleave', () => {
         hideCustomSelect('desktop');
      })
   })
}

export {initCustomSelects};