import * as $ from 'jquery';
import Swiper from 'swiper/bundle';
import {
   numberFormat
} from '../helper';
import { translate } from '../helper/helpers';

export class Profile {
   constructor() {
      this.$profileOrders = $('#profile_orders_list');
      if (!this.$profileOrders.length) return;

      this.isDesktop = window.innerWidth > 1000;

      this.init();
   }

   init() {
      this.getOrders();
   }

   getOrders() {
      const ajaxUrl = `/ajax/profile/`;

      $.ajax({
         url: ajaxUrl,
         type: 'POST',
         dataType: 'JSON',
         data:{
            lang : $("#lang").val(),
            userId : $('#user').val()
         },
         success: (res) => {
            this.successFn(res,'submit');
            this.item = res.item;
         },
         error: (res) => {
            /* res = this.getMockup(); */
            this.successFn(res,'error');
         },
         timeout: 30000
      });
   }

   successFn(res,type) {
      /* console.log('Мокап заказов профиля:', res); */
      if(res?.orders?.length == 0 || !res?.orders?.length || !res || type == 'error'){
         let noOrdersHTML = `<div class="cart-noorders-block">
            <h3 class="noorders-title">${translate('У вас нет заказов')}</h3>
            <p class="body-18 noorders-text">${translate('Найдите то, что вам нужно в каталоге')}</p>
         </div>`
         $('.profile-orders-wrapper').append(noOrdersHTML);
         return false;
      }

      const resultHTML = this.fullRenderOrder(res.orders);

      /* const resultHTML = this.renderOrders(res.orders); */
      
      this.$profileOrders.append(resultHTML);

      this.initHandlers();
   }

   fullRenderOrder(orders) {
      if (!orders) return;

      let result = '';

      orders.forEach(bigOrder =>{
         const {
            globalOrderId='',
            date='',
            soldedOrders = [],
         } = bigOrder;

         let simpleOrderHTML = '';

         simpleOrderHTML = this.renderOrders(soldedOrders);

         result += `
            <div class="full-order-block" data-global-id="${globalOrderId}">
               <div class="full-order-info-block">
                  <div class="info">
                     <span class="full-order-id ">${translate('Заказ')} №${globalOrderId}</span>
                     <span class="full-order-date">${translate('от')} ${date}</span>
                  </div>
                  <div class="buttons hide-print hide-mobile">
                     <a data-global-id="${globalOrderId}" class="button white pdf-button">${translate('Сохранить PDF')}</a>
                     <a href="https://order.primoimperatore.com/profile/?isNaked=1&nc_ctpl=185&lang=${$('#lang').val()}&history=${globalOrderId}" class="button white xls-button">${translate('Сохранить XLS')}</a>
                  </div>
               </div>
               ${simpleOrderHTML}
               <div class="buttons hide-print hide-desktop">
                  <a data-global-id="${globalOrderId}" class="button white pdf-button">${translate('Сохранить PDF')}</a>
                  <a href="https://order.primoimperatore.com/profile/?isNaked=1&nc_ctpl=185&lang=${$('#lang').val()}&history=${globalOrderId}" class="button white xls-button">${translate('Сохранить XLS')}</a>
               </div>
            </div>
         `
      })

      return result;
   }

   renderOrders(orders) {
      if (!orders) return;

      let result = '';
      
      orders.forEach(order => {
         const {
               orderId = 0,
               name = '',
               article = '',
               photos = [],
               items = [],
               commentary = '',
               link=''
         } = order;

         let chosenColorsHTML = '';
         let itemOrderCount = 0;
         let itemOrderSumm = 0;
         
         items.forEach(item => {
            chosenColorsHTML += `
            <div class="chosen-item">
               <div class="chosen-item-left">
                  <div class="chosen-item__color">
                     <div class="round-color"
                        style="background-color:${item.colorHex}; border:${item.colorHex == '#ffffff' || item.colorHex == '#fff'|| item.colorHex == '#FFF'|| item.colorHex == '#FFFFFF' || item.colorHex == 'white' ? '1px solid black' : ''}; ">
                     </div>
                     <span class="round-color-print body-16">${translate('Цвет')}:</span>
                     <span class="round-color__name body-16">${item.colorName}</span>
                  </div>
                  <div class="item-size">
                     <span class="color-gray2 body-16">${translate('Размер')}:</span>
                     <span class="body-16">${item.size}</span>
                  </div>
                  <div class="item-cost hide-desktop">
                     <span class="color-gray2 body-16">${translate('Цена')}:</span>
                     <span data-article="${article}" class="body-16 color-cost">${numberFormat(item.price * item.count,0,'',' ')}
                        €</span>
                  </div>
               </div>
                  <div class="chosen-item-right">
                     <div class="item-cost hide-mobile">
                        <span class="color-gray2 body-16">${translate('Цена')}:</span>
                        <span data-article="${article}" class="body-16 color-cost">${numberFormat(item.price * item.count,0,'',' ')}
                        €</span>
                     </div>
                     <div class="right-block-print">
                        <span class="count-print body-16">${translate('Количество')}:</span>
                        <span class="body-16 order-count"> ${item.count == null || item.count == 'null' ? 0 :item.count} <span class="count-prefix">${translate('шт')}.</span></span>
                     </div>
                  </div>
            </div>
         <div class="hr between-items"></div>
         `

            itemOrderCount += +item.count;
            itemOrderSumm += item.price * item.count;
         })

         result += `

         <div class="cart-order-item" data-order-id="${orderId}">
            <div class="cart-gallery-preview-block">
               <div class="cart-gallery-preview" style="background-image:url(${photos[0]});" data-photos='${photos}'
                  data-open-modal-button="gallery-modal" data-effect-type="open-modal-fade-effect">
                  <div class="big-picture-icon hide-print"></div>
                  <img src="${photos[0]}" class="print-image">
               </div>
               <div class="print-title-block" style="margin-left:20px;">
               <a href="${link}" class="simple-order__head hide-desktop hide-print">${name}: ${translate('Артикул')} ${article}</a>
               <div class="print-order-title">
                  <span class="simple-order__head">${name}</span>
                  <span class="body-16">${translate('Артикул')} ${article}</span>
               </div>
               </div>
            </div>
            <div class="hr mobile hide-desktop"></div>
            <div class="cart-simple-order">
               <a href="${link}" class="simple-order__head hide-mobile hide-print">${name}: ${translate('Артикул')} ${article}</a>
               <div class="hr hide-mobile hr-print"></div>
               <div class="chosen-wrapper">
                  ${chosenColorsHTML}
                  <span class="body-16 profile-commentary hide-print">${translate('Комментарий')}: ${commentary}</span>
                  <div class="print-order-info">
                     <span class="body-16 color-gray4">${translate('Комментарий')}: <span class="body-16 color-black">${commentary}</span></span>
                     <span class="body-16 color-gray4">${translate('Товаров')}: <span class="body-16 color-black">${itemOrderCount}</span></span>
                     <span class="body-16 color-gray4">${translate('Сумма')}: <span class="body-16 color-black">${numberFormat(itemOrderSumm,0,'',' ')} €</span></span>
                  </div>
                  <div class="hr hr-print"></div>
                  <div class="simple-order-info hide-print">
                     <div class="simple-order__count">
                        <span class="headline-4 color-gray4">${translate('Товаров')}:</span>
                        <span class="headline-4 order-count">${itemOrderCount}</span>
                     </div>
                     <div class="simple-order__cost">
                        <span class="headline-4 color-gray4">${translate('Сумма')}:</span>
                        <span class="headline-4 order-sum">${numberFormat(itemOrderSumm,0,'',' ')} €</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>`

      });

      return result;
   }

   initHandlers() {
      //заполнение слайдера фото в зависимости от кликнутой фото в заказе
      $('.cart-gallery-preview').on('click', e=>{
         const currentImage = e.currentTarget;
         $('html').css('overflow','hidden');
         this.initModalItems($(currentImage).data('photos').split(','));

         this.initSlider();
      });

      $('#close_gallery').on('click',()=>{
         $('html').css('overflow','auto');
         $('.swiper-wrapper').empty();
         $('#gallery-modal__slider').find('.swiper-notification').remove();
      });

      $('.pdf-button').on('click', e=>{
         $('.full-order-block').removeClass('dont-print');

         const currentButton = $(e.currentTarget);

         $('.full-order-block').addClass('dont-print');
         currentButton.parents('.full-order-block').removeClass('dont-print');

         window.print();
      });
   }

   getMockup() {
      return {
         "orders": [
            {
               "globalOrderId":"1234",
               "date":"18.02.2021",
               "soldedOrders":[
                  {
                     "orderId":"1",
                     "name": "Футболка",
                     "article": "194944",
                     "photos": ['./img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg',
                        './img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg','./img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg'
                     ],
                     "items": [
                        {
                           "price": "1500",
                           "count": null,
                           "colorHex": "#9fc8e0",
                           "colorName": "Голубой",
                           "colorId":1,
                           "size": "48"
                        },
                        {
                           "price": "1400",
                           "count": "9",
                           "colorHex": "#324563",
                           "colorName": "Темно-синий",
                           "colorId":2,
                           "size": "49"
                        },
                        {
                           "price": "1400",
                           "count": "9",
                           "colorHex": "#324563",
                           "colorName": "Темно-синий",
                           "colorId":2,
                           "size": "49"
                        },
                        {
                           "price": "1400",
                           "count": "9",
                           "colorHex": "#324563",
                           "colorName": "Темно-синий",
                           "colorId":2,
                           "size": "49"
                        },
                        {
                           "price": "1800",
                           "count": "1",
                           "colorHex": "#fff",
                           "colorName": "Белый",
                           "colorId":3,
                           "size": "50"
                        },
                        {
                           "price": "1800",
                           "count": "1",
                           "colorHex": "#fff",
                           "colorName": "Белый",
                           "colorId":3,
                           "size": "50"
                        },
                        {
                           "price": "1800",
                           "count": "1",
                           "colorHex": "#fff",
                           "colorName": "Белый",
                           "colorId":3,
                           "size": "50"
                        },
                        {
                           "price": "1800",
                           "count": "1",
                           "colorHex": "#fff",
                           "colorName": "Белый",
                           "colorId":3,
                           "size": "50"
                        },
                        {
                           "price": "1800",
                           "count": "1",
                           "colorHex": "#fff",
                           "colorName": "Белый",
                           "colorId":3,
                           "size": "50"
                        }
                     ],
                     "commentary": "Хочу все завтра!"
                  },
                  {
                     "orderId":"2",
                     "date":"17.02.2021",
                     "name": "Джинсы",
                     "article": "999999",
                     "photos": ['./img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg',
                        './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg', './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg'
                     ],
                     "items": [
                        {
                           "price": "4500",
                           "count": "3",
                           "colorHex": "#ff0000",
                           "colorName": "Красный",
                           "colorId":7,
                           "size": "48"
                        },
                        {
                           "price": "5000",
                           "count": "1",
                           "colorHex": "#d4d4d4",
                           "colorName": "Серый",
                           "colorId":9,
                           "size": "52"
                        },
                        {
                           "price": "4500",
                           "count": "3",
                           "colorHex": "#ff0000",
                           "colorName": "Красный",
                           "colorId":7,
                           "size": "48"
                        },
                        {
                           "price": "4500",
                           "count": "3",
                           "colorHex": "#ff0000",
                           "colorName": "Красный",
                           "colorId":7,
                           "size": "48"
                        },
                        {
                           "price": "4500",
                           "count": "3",
                           "colorHex": "#ff0000",
                           "colorName": "Красный",
                           "colorId":7,
                           "size": "48"
                        }
                     ]
                  }
               ]
            },
            {
               "globalOrderId":"1235",
               "date":"17.02.2021",
               "soldedOrders":[
                  {
                     "orderId":"2",
                     "date":"17.02.2021",
                     "name": "Джинсы",
                     "article": "999999",
                     "photos": ['./img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg',
                        './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg', './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg'
                     ],
                     "items": [
                        {
                           "price": "4500",
                           "count": "3",
                           "colorHex": "#ff0000",
                           "colorName": "Красный",
                           "colorId":7,
                           "size": "48"
                        },
                        {
                           "price": "5000",
                           "count": "1",
                           "colorHex": "#d4d4d4",
                           "colorName": "Серый",
                           "colorId":9,
                           "size": "52"
                        }
                     ]
                  }
               ]
            }
         ]
      }
   }

   initSlider() {
      this.$sliderInstance = new Swiper('#gallery-modal__slider', {
         effect: 'slide',
         direction: 'horizontal',
         loop: false,
         preloadImages: false,
         lazy: true,
         slidesPerView: 1,
         resistance: false,
         allowTouchMove: true,
         spaceBetween: 0,
         navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
         }
      });
   };

   initModalItems(imageArray) {
      let template = ``;

      imageArray.forEach((item) => {
         template += this.getModalItemTemplate(item);
      });


      $('.swiper-wrapper').html(template);
   };

   getModalItemTemplate(image) {
      return `
         <div class="swiper-slide" style="background-image: url(${image})"></div>
      `;
   };

}