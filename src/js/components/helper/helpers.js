/**
 * Форматирования числа в денежный формат
 * @param {Number} number - число для форматирования
 * @param {Number} decimals - знаков после запятой
 * @param {String} decPoint - разделитель для float
 * @param {String} thousandsSep - разделить разрядов
 * @returns {string} число в денежном формате
 */
function numberFormat(number, decimals = 0, decPoint = '', thousandsSep = ' ') {
    let i,
        j,
        kw,
        kd,
        km;

    i = parseInt(number = (+number || 0).toFixed(decimals)) + '';
    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }
    km = (j ? i.substr(0, j) + thousandsSep : '');
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + thousandsSep);
    kd = (decimals ? decPoint + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : '');
    return km + kw + kd;
}

function refreshForm() {
    let globalCount = 0;
    let globalPrice = 0;
    let localPrice = 0;
    let localCount = 0;

    $('.final-price').find('button').removeClass('inactive');

    $('#chose_item_table').find('.color-row').each((i, item) => {
        const $switcher = $(item).find('.switcher-gray');

        $(item).find('input.input-count').each((index, input) => {

            if (+input.value !== 0 || input.value !== '') {
                globalCount += +input.value;
            }
            localPrice += +input.value * input.dataset.price;
            localCount += +input.value;
            $(item).find('.final').text(`${getRightPrice(localPrice)} €`);
            $(item).find('.amount').text(localCount);
        })

        globalPrice += localPrice;
        localPrice = 0;
        localCount = 0;
    })

    const $activeSwitchers = $('tr.color-row').find('.switcher-gray.active');

    if(!$activeSwitchers.length){
        $('.final-price').find('button').addClass('inactive');
    }

    $('#choose_sum').text(`${getRightPrice(globalPrice)} € `);
    $('#choose_count').text(globalCount);
}

let dictionary = {
    'Артикул':{
        'ru':'Артикул',
        'en':'Аrticle',
        'it':'Articolo'
    },
    'Каталог':{
        'ru':'Каталог',
        'en':'Catalog',
        'it':'Catalogare'
    },
    'стоимость зависит от размера':{
        'ru':'стоимость зависит от размера',
        'en':'the cost depends on the size',
        'it':'il costo dipende dalle dimensioni'
    },
    'Состав':{
        'ru':'Состав',
        'en':'Composition',
        'it':'Composizione'
    },
    'Коллекция':{
        'ru':'Коллекция',
        'en':'Collection',
        'it':'Collezione'
    },
    'Доступные цвета':{
        'ru':'Доступные цвета',
        'en':'Available colors',
        'it':'Colori disponibili'
    },
    'Описание':{
        'ru':'Описание',
        'en':'Description',
        'it':'Descrizione'
    },
    'Выбрать размер, цвет и количество':{
        'ru':'Выбрать размер, цвет и количество',
        'en':'Choose size, color and quantity',
        'it':'Scegli taglia, colore e quantità'
    },
    'Размер':{
        'ru':'Размер',
        'en':'Size',
        'it':'Dimensione'
    },
    'шт':{
        'ru':'шт',
        'en':'pcs',
        'it':'pc'
    },
    'Стоимость':{
        'ru':'Стоимость',
        'en':'Price',
        'it':'Prezzo'
    },
    'Комментарий':{
        'ru':'Комментарий',
        'en':'Commentary',
        'it':'Commento'
    },
    'Итого':{
        'ru':'Итого',
        'en':'Total',
        'it':'Totale'
    },
    'Товаров':{
        'ru':'Товаров',
        'en':'Goods',
        'it':'Merce'
    },
    'В корзину':{
        'ru':'В корзину',
        'en':'Add to cart',
        'it':'Aggiungi al carrello'
    },
    'Отлично':{
        'ru':'Отлично',
        'en':'Fine',
        'it':'Bene'
    },
    'Ваш товар добавлен в корзину':{
        'ru':'Ваш товар добавлен в корзину',
        'en':'Your item has been added to your cart',
        'it':'il tuo articolo è stato aggiunto al carrello'
    },
    'Перейти в корзину':{
        'ru':'Перейти в корзину',
        'en':'Go to cart',
        'it':'Vai al carrello'
    },
    'Ошибка':{
        'ru':'Ошибка',
        'en':'Error',
        'it':'Errore'
    },
    'Попробуйте ещё раз':{
        'ru':'Попробуйте ещё раз',
        'en':'Try again',
        'it':'Riprova'
    },
    'Перейти в каталог':{
        'ru':'Перейти в каталог',
        'en':'Go to catalog',
        'it':'Vai al catalogo'
    },
    'В корзине нет товаров':{
        'ru':'В корзине нет товаров',
        'en':'There are no items in your cart',
        'it':'Non ci sono articoli nel carrello'
    },
    'Сумма':{
        'ru':'Сумма',
        'en':'Sum',
        'it':'Somma'
    },
    'Цена':{
        'ru':'Цена',
        'en':'Price',
        'it':'Prezzo'
    },
    'Найдите то, что вам нужно в каталоге':{
        'ru':`Найдите то, что вам нужно <a class="noorders-link" href="/">в каталоге</a>`,
        'en':`Find what you need <a class="noorders-link" href="/en">in the catalog</a>`,
        'it':`Trova ciò di cui hai bisogno <a class="noorders-link" href="/it">nel catalogo</a>`
    },
    'Вход':{
        'ru':'Вход',
        'en':'Entrance',
        'it':'Ingresso'
    },
    'Войти':{
        'ru':'Войти',
        'en':'Enter',
        'it':'Entrare'
    },
    'Восстановить пароль':{
        'ru':'Восстановить пароль',
        'en':'Restore password',
        'it':'Ripristina password'
    },
    'Закрыть':{
        'ru':'Закрыть',
        'en':'Close',
        'it':'Chiudere'
    },
    'Получить пароль':{
        'ru':'Получить пароль',
        'en':'Get password',
        'it':'Ottenere la password'
    },
    'От':{
        'ru':'От',
        'en':'From',
        'it':'Da'
    },
    'от':{
        'ru':'от',
        'en':'from',
        'it':'da'
    },
    'Назад':{
        'ru':'Назад',
        'en':'Back',
        'it':'Ritorno'
    },
    'Заказ успешно отправлен менеджеру магазина':{
        'ru':'Заказ успешно отправлен менеджеру магазина',
        'en':'The order has been successfully sent to the store manager',
        'it':`L'ordine è stato inviato con successo al responsabile del negozio`
    },
    'Продолжить покупки':{
        'ru':'Продолжить покупки',
        'en':'Continue shopping',
        'it':'Continua a fare acquisti'
    },
    'Количество':{
        'ru':'Количество',
        'en':'Quantity',
        'it':'Quantità'
    },
    'Цвет':{
        'ru':'Цвет',
        'en':'Color',
        'it':'Colore'
    },
    'У вас нет заказов':{
        'ru':'У вас нет заказов',
        'en':'You have no orders',
        'it':'Non hai ordini'
    },
    'Заказ':{
        'ru':'Заказ',
        'en':'Order',
        'it':'Ordine'
    },
    'Сохранить PDF':{
        'ru':'Сохранить PDF',
        'en':'Save PDF',
        'it':'Salva PDF'
    },
    'Сохранить XLS':{
        'ru':'Сохранить XLS',
        'en':'Save XLS',
        'it':'Salva XLS'
    },
    'Ой, что-то пошло не так':{
        'ru':'Ой, что-то пошло не так',
        'en':'Oh something went wrong',
        'it':'Oh qualcosa è andato storto'
    },
    'Пользователь с таким email не существует':{
        'ru':'Пользователя с таким email не существует',
        'en':'User with this email does not exist',
        'it':"L'utente con questa email non esiste"
    },
    'Пароль отправлен':{
        'ru':'Пароль отправлен',
        'en':'Password sent',
        'it':'Password inviata'
    },
    'Проверьте вашу почту':{
        'ru':'Проверьте вашу почту',
        'en':'Check your email',
        'it':'Controlla la tua email'
    }
}

function translate(word){
    const lang = $('#lang').val();
    return dictionary[word][lang]
}

function formatDate() {
    let date = new Date();

    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;
  
    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;
  
    var yy = date.getFullYear() % 100;
    if (yy < 10) yy = '0' + yy;
  
    return dd + '.' + mm + '.' + yy;
}

function getRightPrice(price){
    //если число целое - возвращаем без разделителей
    if(Number.isInteger(price)){
        return numberFormat(price,'','',' ' );

        //иначе - с копейками
    }else{
        return numberFormat(price,2,',',' ' );
    }
}

export {
    numberFormat,
    refreshForm,
    translate,
    formatDate,
    getRightPrice
};