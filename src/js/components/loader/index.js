import * as $ from 'jquery';

function showLoader(form,loader){
   form.addClass('hide');
   loader.removeClass('hide');
}

function hideLoader(loader){
   if(loader == $('#restore-loader')){
      $('.restore-wrapper').addClass('error');

   }else if(loader == $('#login-loader')){
      $('.login-wrapper').addClass('error');
   }

   loader.addClass('hide');
}

export {showLoader,hideLoader};