import * as $ from 'jquery';
import { initFormWithValidate } from "../form";
import {showLoader, hideLoader} from "../loader";
import { translate } from '../helper/helpers';

export default class InitRestoreForm{
   constructor(){
      this.$form = $('#restore_form');
      this.$formMessageBlock = this.$form.find('.form-message');
      
      if (!this.$form.length) return false;
      
      this.successForm = this.successForm.bind(this);
      this.errorForm = this.errorForm.bind(this);
      this.clearForm = this.clearForm.bind(this);

      this.init();
   }

   init(){
      initFormWithValidate(this.$form);
      this.submit(this.successForm, this.errorForm);
   }

   submit(successFn, errorFn) {
      this.$form.on('submit',  (e)=> {
         e.preventDefault();

         let $formData = {};

         $(this).find('input, textarea, select').each( ()=> {
            $formData[this.name] = $(this).val();
         });

         let data = {
            sub: 100,
            cc: 100,
            login: $formData.restore_form_name
         };

         data=$('#restore_form').serializeArray();

         showLoader($('#restore_form'),$('#restore_loader'));

         $.ajax({
            url: $('#restore_form').data('request'),
            type: 'POST',
            dataType: 'text',
            data: data,
            success:  (res) => {
               successFn(res)
            },
            error:  (res) => {

               /* setTimeout для примера прелоадера */
               setTimeout(() => {
                  errorFn(res);
               }, 3000);
            },
            timeout: 30000
         });
      })
   }

   successForm(res) {
      this.clearForm();
      let response = JSON.parse(res)
      
      if(response.success === false){
         this.errorForm(response);

      }else{
         $('#restore_loader').addClass('hide');
         $('.restore-wrapper').addClass('success');
   
         $('.form-message-success').removeClass('hide');
         $('.form-message-success').find('.message-title').text(translate('Пароль отправлен'));
         $('.form-message-success').find('.message-subtitle').text(translate('Проверьте вашу почту'));
         $('.form-message-success').find('a.button').text(translate('Войти'));
      }
   }

   errorForm(res) {

      /* res для выведения ошибки в message-title и message-subtitle*/
      this.clearForm();

      hideLoader($('#restore_loader'));

      let errorText = typeof translate(res.errors) == 'undefined' ? translate('Ой, что-то пошло не так') : translate(res.errors);

      $('.form-message').removeClass('hide');
      $('.form-message').find('.message-title').text(translate('Ошибка'));
      $('.form-message').find('.message-subtitle').text(errorText);
      $('.form-message').find('#close_error').text(translate('Закрыть'));

      $('.form-message').find('#close_error')
      .on('click', () => {
         $('.form-message').addClass('hide');
         $('.restore-wrapper').removeClass('error');
         $('#restore_form').removeClass('hide');
      })
   }

   clearForm() {
      this.$form[0].reset();
      this.$form.find('.field').removeClass('success').addClass('empty');
   }
}