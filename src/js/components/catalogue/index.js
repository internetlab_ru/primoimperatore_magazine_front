import * as $ from 'jquery';
import {
   numberFormat
} from '../helper';
import { translate } from '../helper/helpers';
import lozad from 'lozad';

export class Catalogue {
   constructor() {
      if (!$('#catalogue_list_block').length) return;

      this.$catalogue = $('#catalogue_list_block');
      this.$filter = $('.catalogue-filters');

      this.catalogueFilters = [];

      //Lazyload
      this.observer = lozad();

      this.init();
   }

   init() {
      this.observer.observe();

      this.getList();

      this.$filter.on('click', '.filter-item', (e) => {
         const $this = e.currentTarget;
         const data_filter = $this.dataset['filter'];
         const firstButton = this.$filter.find(".filter-item[data-filter='0']");
         const activeButton = this.$filter.find(".filter-item.active"); 

         $this.classList.toggle('active');

         if (data_filter === '0') {
            $($this).siblings().removeClass('active')
            this.catalogueFilters = [];

            //клик по Все, если активна только она, не отключает кнопку

            if (!$this.classList.contains('active')) {
               $($this).addClass('active');
            }

         //Если клик не по кнопке Все и кнопка Все активна -> убираем активный класс у Все и добавлем таргету

         } else if (data_filter !== '0' && activeButton.attr('data-filter') === '0') {
            $('.filter-item.active').removeClass('active');
            $($this).addClass('active');

         //Если клик не по кнопке Все, кнопка Все не выбрана и все остальные кнопки не активны -> ставим активный класс кнопке Все

         } else if (data_filter !== '0' && !firstButton.hasClass('active') &&
            this.$filter.find(".filter-item.active").length == 0) {

            firstButton.addClass('active');
         //Если клик не по кнопке Все, кнопка Все не выбрана и кликаем по оставшейся не выбранной кнопке -> ставим активный класс кнопке Все
         
         } else if (data_filter !== '0' && !firstButton.hasClass('active') && activeButton.length > 4) {
            firstButton.siblings().removeClass('active');
            firstButton.addClass('active');
         } else {
            $($this).siblings('[data-room="0"]').removeClass('active');
            if (this.catalogueFilters.includes(0)) {
               this.catalogueFilters.splice(0)
            }
         }

         let test = [];

         this.$filter.find(".filter-item.active").each(function () {
            test.push(+$(this).attr('data-filter'));
         });

         this.catalogueFilters = test;

         this.filterCatalogue();

         return false;
      });

   }

   /**
    * Фильтрация каталога
    */
   filterCatalogue() {
      if (!$(document).find('.catalogue-card').length) {
         return false;
      }

      const catalogueItems = $('.catalogue-card');

      let filterArray = this.catalogueFilters;

      catalogueItems.each(function(key){
         const obj = catalogueItems[key];
         const filter = +$(obj).data('filter') || [];

         if (filterArray.length) {
            if (filterArray.includes(0)) {
               $(obj).removeClass('hide');
            } else {
              
               filterArray.sort((a, b) => +a > +b ? 1 : -1);
               for (let i = 0; i < filterArray.length; i++) {
                  if (filter) {
                     if (filter == filterArray[i]) {
                        $(obj).removeClass('hide');
                        break;
                     } else {
                        $(obj).addClass('hide');
                     }
                  }
               }
            }

         } else {
            $(obj).removeClass('hide');
         }
      })
   }

   /* ajax за каталогом */

   getList() {
      const ajaxUrl = `/ajax/catalog/`;

      this.isLoading = true;

      $.ajax({
         url: ajaxUrl,
         type: 'POST',
         dataType: 'json',
         data:{
            lang : $("#lang").val()
         },
         success: (res) => {
            this.successFn(res);
            this.cards = res.list;
         },
         error: (res) => {
           /*  res = this.getMockup(); */
            this.successFn(res);
            /* console.log(res); */
         },
         timeout: 30000
      });
   }

   successFn(res) {
      const resultHTML = this.getCardsHTML(res.list);

      this.$catalogue.append(resultHTML.result)

      this.observer.observe();
   }

   getCardsHTML(array) {
      if (!array) return;

      let result = '';

      array.forEach((item) => {
         result += renderCard(item);
      })

      return {
         result
      };

      function renderCard(item) {
         if (!item) return;
         const {
            id = 0,
               name = '',
               flag = '',
               article = '',
               photo = '',
               link = '',
               price = '',
         } = item;

         let result = ` 
         <div class="catalogue-card" data-image="${photo}" data-filter="${flag}">
         <a href="${link}">
        <div class="upper-card-wrapper"> 
        <div class="lozad catalogue-card__image" style="background-image:url(${photo});" data-src="${photo}" alt=""></div>
        <p class="catalogue-card__name">${name}</p>
        <span class="catalogue-card__description">${translate('Артикул')}: ${article} </span>
        </div>
         <div class="hr-and-price-wrapper">
         <div class="hr-dashed"></div>
         <div class="catalogue-card__price-block">
         
            <span class="headline-5">от ${numberFormat(price,0,'',' ')} €</span>
            <i class="icon icon-arrow-down"></i>
         </div>
         </div>
            </a>
         </div>
         `
         return result;
      }
   }

   getMockup() {
      return {
         "list": [
            {
               "id": "1",
               "name": "Футболка",
               "flag": "6",
               "article": "194940",
               "photo": "img/test-photos/man.jpg",
               "link": "/tshirt",
               "price": "1500",
            },
            {
               "id": "2",
               "name": "Футболка",
               "flag": "6",
               "article": "194940",
               "photo": "img/test-photos/man-tshirt2.jpg",
               "link": "/tshirt",
               "price": "1700",
            },
            {
               "id": "3",
               "name": "Футболка",
               "flag": "6",
               "article": "194940",
               "photo": "img/test-photos/man-tshirt2.jpg",
               "link": "/tshirt",
               "price": "1800",
            },
            {
               "id": "4",
               "name": "Рубашка",
               "flag": "2",
               "article": "194941",
               "photo": "img/test-photos/man-polo1.jpg",
               "link": "/tshirt",
               "price": "1800",
            },
            {
               "id": "5",
               "name": "Рубашка",
               "flag": "2",
               "article": "194941",
               "photo": "img/test-photos/man-polo1.jpg",
               "link": "/tshirt",
               "price": "1100",
            },
            {
               "id": "6",
               "name": "Куртка",
               "flag": "3",
               "article": "194942",
               "photo": "img/test-photos/man.jpg",
               "link": "/tshirt",
               "price": "1800",
            },
            {
               "id": "7",
               "name": "Джинсы",
               "flag": "4",
               "article": "194943",
               "photo": "img/test-photos/man-jeans.jpg",
               "link": "/tshirt",
               "price": "1800",
            },
            {
               "id": "8",
               "name": "Шорты",
               "flag": "4",
               "article": "194944",
               "photo": "img/test-photos/man-jeans.jpg",
               "link": "/tshirt",
               "price": "1800",
            },
            {
               "id": "9",
               "name": "Спортивный костюм",
               "flag": "5",
               "article": "194945",
               "photo": "img/test-photos/man.jpg",
               "link": "/tshirt",
               "price": "10800",
            },
            {
               "id": "10",
               "name": "Поло",
               "flag": "1",
               "article": "194946",
               "photo": "img/test-photos/man-polo2.jpg",
               "link": "/tshirt",
               "price": "1080",
            },
            {
               "id": "11",
               "name": "Поло",
               "flag": "1",
               "article": "194946",
               "photo": "img/test-photos/man-polo2.jpg",
               "link": "/tshirt",
               "price": "1080",
            },
            {
               "id": "12",
               "name": "Спортивный костюм",
               "flag": "5",
               "article": "194945",
               "photo": "img/test-photos/man.jpg",
               "link": "/tshirt",
               "price": "13800",
            },
            {
               "id": "13",
               "name": "Спортивный костюм",
               "flag": "5",
               "article": "194945",
               "photo": "img/test-photos/man.jpg",
               "link": "/tshirt",
               "price": "19999",
            },
         ]
      }
   }
}