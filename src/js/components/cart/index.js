import * as $ from 'jquery';
import Swiper from 'swiper/bundle';
import {
   numberFormat
} from '../helper';
import {getRightPrice, translate} from '../helper/helpers';
import { initPlaceholders } from '../form';


export class Cart {
   constructor() {
      this.$orders = $('#orders_list');
      if (!this.$orders.length) return;
      this.isDesktop = window.innerWidth > 1000;

      this.currentHeaderCount = 0;
      this.init();
   }

   init() {
      this.getOrders();
   }

   getOrders() {
      const ajaxUrl = `/ajax/cart/`;

      $.ajax({
         url: ajaxUrl,
         type: 'POST',
         dataType: 'JSON',
         data:{
            lang : $("#lang").val(),
            userId : $('#user').val()
         },
         success: (res) => {
            this.successFn(res ,'success');
            this.item = res.item;
         },
         error: (res) => {
           /*  res = this.getMockup(); */
            this.successFn(res, 'error');
         },
         timeout: 30000
      });
   }

   successFn(res,type) {
      /* console.log('Мокап заказов корзины:', res) */

      if(res?.orders?.length == 0 || !res?.orders?.length || !res || type == 'error'){
         $('.cart-orders-info').remove();
         let noOrdersHTML = `<div class="cart-noorders-block">
            <h3 class="noorders-title">${translate('В корзине нет товаров')}</h3>
            <p class="body-18 noorders-text">${translate('Найдите то, что вам нужно в каталоге')}</p>
         </div>`
         $('.cart-wrapper').append(noOrdersHTML);
         return false;
      }
      const resultHTML = this.renderOrders(res.orders);
      this.orderId = res.orderId;
      
      this.$orders.append(resultHTML);

      this.initHandlers();
   }

   renderOrders(orders) {
      if (!orders) return;

      let result ='';
      let fullCount= 0;
      let fullPrice=0;

      orders.forEach(order => {
         const {
               orderId = 0,
               name = '',
               article = '',
               photos = [],
               items = [],
               commentary = '',
               link=''
         } = order;

         let chosenColorsHTML = '';
         let itemOrderCount = 0;
         let itemOrderSumm = 0;
         
         items.forEach(item => {
            let price = +item.price;

            if(!Number.isInteger(price)){
               price = +item.price.replace(',', '.');
            }

            chosenColorsHTML += `
            <div class="chosen-item">
                 <div class="chosen-item-left">
                  <div class="chosen-item__color">
                     <div class="round-color"
                        style="background-color:${item.colorHex}; border:${item.colorHex == '#ffffff' || item.colorHex == '#fff'|| item.colorHex == '#FFF'|| item.colorHex == '#FFFFFF' || item.colorHex == 'white' ? '1px solid black' : ''}; ">
                     </div>
                     <span class="round-color-print body-16">${translate('Цвет')}:</span>
                     <span class="round-color__name body-16">${item.colorName}</span>
                  </div>
                  <div class="item-size">
                     <span class="color-gray2 body-16">${translate('Размер')}:</span>
                     <span class="body-16">${item.size}</span>
                  </div>
                  <div class="item-cost hide-desktop">
                     <span class="color-gray2 body-16">${translate('Цена')}:</span>
                     <span data-article="${article}" class="body-16 color-cost">${getRightPrice(price * item.count)}
                        €</span>
                  </div>
                 </div>
                  <div class="chosen-item-right">
                     <div class="item-cost hide-mobile">
                        <span class="color-gray2 body-16">${translate('Цена')}:</span>
                        <span data-article="${article}" class="body-16 color-cost">${getRightPrice(price * item.count)}
                        €</span>
                     </div>
                     <div class="right-block-print">
                        <span class="count-print body-16">${translate('Количество')}:</span>
                        <label class="field field-count numberic placeholder">
                           <input class="input-count item placeholder" data-size="${item.size}" data-placeholder="0" placeholder="0"
                              maxLength="4" data-color-id="${item.colorId}" data-atribute-id="${item.atributeId}" data-article="${article}" data-price="${price}" type="tel"
                              value="${item.count == null || item.count == 'null' ? 0 :item.count}" data-order-id="${orderId}">
                           <span>${translate('шт')}</span>
                        </label>
                     </div>
                     <div class="remove-item"><i class="icon icon-close"></i></div>
                  </div>
            </div>
         <div class="hr between-items"></div>
         `

            itemOrderCount += +item.count;
            itemOrderSumm += price * item.count;
         })

         result += `
         
         <div class="cart-order-item" data-order-id="${orderId}">
            <div class="cart-gallery-preview-block">
               <div class="cart-gallery-preview" style="background-image:url(${photos[0]});" data-photos='${photos}'
                  data-open-modal-button="gallery-modal" data-effect-type="open-modal-fade-effect">
                  <div class="big-picture-icon hide-print"></div>
                  <img src="${photos[0]}" class="print-image">
               </div>
               <div class="print-title-block" style="margin-left:20px;">
               <a href="${link}" class="simple-order__head hide-desktop hide-print">${name}: ${translate('Артикул')} ${article}</a>
               <div class="print-order-title">
                  <span class="simple-order__head">${name}</span>
                  <span class="body-16">${translate('Артикул')} ${article}</span>
               </div>
               </div>
            </div>
            <div class="hr mobile hide-desktop"></div>
            <div class="cart-simple-order">
               <a href="${link}" class="simple-order__head hide-mobile hide-print">${name}: ${translate('Артикул')} ${article}</a>
               <div class="hr hide-mobile"></div>
               <div class="chosen-wrapper">
                  ${chosenColorsHTML}
                  <label class="field field-input item email-label hide-print">
                     <input class="input-text placeholder" name="choose_commentary" data-article="${article}"
                        data-placeholder="${translate('Комментарий')}" value="${commentary}" type="text">
                  </label>
                  <div class="print-order-info">
                     <span class="body-16 color-gray4">${translate('Комментарий')}: <span class="body-16 color-black">${commentary}</span></span>
                     <span class="body-16 color-gray4">${translate('Товаров')}: <span class="body-16 color-black">${itemOrderCount}</span></span>
                     <span class="body-16 color-gray4">${translate('Сумма')}: <span class="body-16 color-black">${getRightPrice(itemOrderSumm)} €</span></span>
                  </div>
                  <div class="hr hr-print"></div>
                  <div class="simple-order-info hide-print">
                     <div class="simple-order__count">
                        <span class="headline-4 color-gray4">${translate('Товаров')}:</span>
                        <span class="headline-4 order-count">${itemOrderCount}</span>
                     </div>
                     <div class="simple-order__cost">
                        <span class="headline-4 color-gray4">${translate('Сумма')}:</span>
                        <span class="headline-4 order-sum">${getRightPrice(itemOrderSumm)} €</span>
                     </div>
                  </div>
               </div>
            </div>
      </div>`

      fullCount += +itemOrderCount;
      fullPrice += +itemOrderSumm;
      });

      $('#orders_count').text(fullCount);
      $('#orders_price').text(`${getRightPrice(fullPrice)} €`);
      /* $('.print-date').text(`от ${formatDate()}`); */

      if($('#orders_count').text() == '0'){
         $('.additional-buttons a.button.white').addClass('inactive');
      }else{
         $('.additional-buttons a.button.white').removeClass('inactive');
      }

      return result;
   }

   initHandlers() {
      initPlaceholders();
      
      //заполнение слайдера фото в зависимости от кликнутой фото в заказе
      $('.cart-gallery-preview').on('click', e=>{
         const currentImage = e.currentTarget;
         $('html').css('overflow','hidden');
         this.initModalItems($(currentImage).data('photos').split(','));

         this.initSlider();
      });

      $('#close_gallery').on('click',()=>{
         $('html').css('overflow','auto');
         $('.swiper-wrapper').empty();
         $('#gallery-modal__slider').find('.swiper-notification').remove();
      });

      $('input.input-count').on('change', e=>{
         this.refreshOrdersInfo(e);
      });

      $('.remove-item').on('click', e=>{
         this.removeProduct(e);
      });

      $('input.input-count, input.input-text').on('change', e=>{
         this.ajaxOrder(e,'edit');
      });

      $('#submit_order').on('click', e=>{
         this.ajaxOrder(e,'submit');
      });

      $('#pdf_button').on('click', ()=>{
         window.print();
      });
   }

   refreshOrdersInfo(e){
      const changedInput = $(e.currentTarget);
      const colorPrice = $(e.currentTarget).data('price');
      const itemArticle = $(e.currentTarget).data('article');
      const parentBlock = $(changedInput).parents('.chosen-item');

      let countSimpleOrder = 0;
      let sumSimpleOrder = 0;

      let countOrders = 0;
      let sumOrders = 0;

      parentBlock.find('.color-cost').text(`${getRightPrice(changedInput.val() * colorPrice)} €`)

      $(`input.input-count[data-article='${itemArticle}']`).each(function(){
         countSimpleOrder += +$(this).val();
         sumSimpleOrder += +$(this).val() * +$(this).data('price');
      })
     
      $(e.currentTarget).parents('.chosen-wrapper').find('.headline-4.order-count').text(`${countSimpleOrder}`)
      $(e.currentTarget).parents('.chosen-wrapper').find('.headline-4.order-sum').text(` ${getRightPrice(sumSimpleOrder)} €`)

      $(`input.input-count`).each(function(){
         countOrders += +$(this).val();
         sumOrders += +$(this).val() * +$(this).data('price');
      });

      $('#orders_count').text(countOrders);
      $('#orders_price').text(`${getRightPrice(sumOrders)} €`);

      if($('#orders_count').text() == '0'){
         $('.additional-buttons a.button.white').addClass('inactive');
      }else{
         $('.additional-buttons a.button.white').removeClass('inactive');
      }
   }

   removeProduct(e){
      let closeInputToIcon =  $(e.currentTarget).parents('.chosen-item').find('input.input-count');

      $(closeInputToIcon).val(0);
      $(closeInputToIcon).data('deleted','true');
      $(closeInputToIcon).trigger('change');
      
      if($(e.currentTarget).parents('.chosen-wrapper').find('.chosen-item').length == 1){
         $(e.currentTarget).parents('.cart-order-item').remove();
      }
      
      $(e.currentTarget).parents('.chosen-item').next('.hr.between-items').remove();
      $(e.currentTarget).parents('.chosen-item').remove();

      if($('.cart-order-item').length == 0){
         $('.cart-orders-info').remove();
         let noOrdersHTML = `<div class="cart-noorders-block">
            <h3 class="noorders-title">${translate('В корзине нет товаров')}</h3>
            <p class="body-18 noorders-text">${translate('Найдите то, что вам нужно в каталоге')}</p>
            </div>`
         $('.cart-wrapper').append(noOrdersHTML);
      }
   }

   ajaxOrder(e,type){
      e.preventDefault();
      let ajaxUrl = '';
      let data = this.collectOrders();

      if(type=='submit'){
         ajaxUrl = '/ajax/submitCart/'
         data={}
         data= {
            lang :$('#lang').val(),
            userId : $('#user').val(),
            sold : 1
         }
         this.startPreloaderWithModal();
         $('.header.hide-mobile').css('z-index', '9');

      } else if(type=='edit'){
         ajaxUrl = '/ajax/editCart/';
         data.userId = $('#user').val();
         data = JSON.stringify(data);
      }

      $.ajax({
         url: ajaxUrl,
         type: 'POST',
         dataType: 'text',
         data:data,
         success: (res) => {
           this.successAjax(res,type)
         },
         error: (res) => {
            this.errorAjax(res,type)
         },
         timeout: 30000
      });
   }

   successAjax(res,type){
      if(type=='submit'){
         this.stopPreloader('ok');
      }else if(type=='edit'){
         $('.header-ui__count').text(this.currentHeaderCount);
      }
   }

   errorAjax(res,type){
      if(type=='submit'){
         this.stopPreloader('error');
      }
   }

   collectOrders(){
      this.currentHeaderCount = 0;

      let data ={
         userId : $('#user-id').val(),
         products : []
      };

      $('.cart-order-item').each((index, order)=>{
         data.products.push(this.collectOrderItem(order));
      })

      return data;
   }

   collectOrderItem(order){
      let result = {
         article : $(order).find('.input-count').data('article'),
         commentary : $(order).find('input.input-text').val(),
         orderId: $(order).find('.input-count').data('order-id'),
         items : []
      };

      $(order).find('.input-count').each((i,item)=>{

         if($(item).data('deleted') == 'true'){
            return;
         }

         let simpleColorObj = {
            atributeId : $(item).data('atribute-id'),
            colorId : $(item).data('color-id'),
            size : $(item).data('size'),
            count : +$(item).val(),
            price : $(item).data('price'),
         }

         this.currentHeaderCount += +$(item).val();

         result.items.push(simpleColorObj);
      });

      return result;
   }

   getMockup() {
      return {
         "userId":'voobshehz',
         "orders": [
            {
               "orderId":"1",
               "name": "Футболка",
               "article": "194944",
               "link":"#",
               "photos": ['./img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg',
                  './img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg','./img/test-photos/big-man2.jpg', './img/test-photos/big-man.jpg'
               ],
               "items": [
                  {
                     "price": "1500",
                     "count": null,
                     "colorHex": "#9fc8e0",
                     "colorName": "Голубой",
                     "colorId":1,
                     "size": "48"
                  },
                  {
                     "price": "1400,1",
                     "count": "9",
                     "colorHex": "#324563",
                     "colorName": "Темно-синий",
                     "colorId":2,
                     "size": "49"
                  },
                  {
                     "price": "1800",
                     "count": "1",
                     "colorHex": "#fff",
                     "colorName": "Белый",
                     "colorId":3,
                     "size": "50"
                  }
               ],
               "commentary": "Хочу все завтра!"
            },
            {
               "orderId":"2",
               "name": "Джинсы",
               "article": "999999",
               "photos": ['./img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg',
                  './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg', './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg'
               ],
               "items": [{
                     "price": "4500",
                     "count": "3",
                     "colorHex": "#ff0000",
                     "colorName": "Красный",
                     "colorId":7,
                     "size": "48"
                  },
                  {
                     "price": "5000",
                     "count": "1",
                     "colorHex": "#d4d4d4",
                     "colorName": "Серый",
                     "colorId":9,
                     "size": "52"
                  }
               ]
            }
         ]
      }
   }

   initSlider() {
      this.$sliderInstance = new Swiper('#gallery-modal__slider', {
         effect: 'slide',
         direction: 'horizontal',
         loop: false,
         preloadImages: false,
         lazy: true,
         slidesPerView: 1,
         resistance: false,
         allowTouchMove: true,
         spaceBetween: 0,
         navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
         }
      });
   };

   initModalItems(imageArray) {
      let template = ``;

      imageArray.forEach((item) => {
         template += this.getModalItemTemplate(item);
      });


      $('.swiper-wrapper').html(template);
   };

   getModalItemTemplate(image) {
      return `
         <div class="swiper-slide" style="background-image: url(${image})"></div>
      `;
   };

   startPreloaderWithModal() {
      let modalHTML = `
         <div class="modal-after-product cart" id="modal_status_add_product">
         <div class="modal-content">
         <div class="form-message-success cool hide">
         <div class="form-info-success">
            <div class="icon-block"><i class="icon icon-checked"></i></div>
            <h4 class="message-title">${translate('Отлично')}</h4>
            <p class="message-subtitle">${translate('Заказ успешно отправлен менеджеру магазина')}</p>
            <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '/': `/${$('#lang').val()}`}" class="button link-to-cart">${translate('Перейти в каталог')}</a>
         </div>
         </div>
         <div class="form-message-success error hide">
         <div class="form-info-success">
            <div class="icon-block error"><i class="icon icon-close"></i></div>
            <h4 class="message-title">${translate('Ошибка')}</h4>
            <p class="message-subtitle">${translate('Попробуйте ещё раз')}</p>
            <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '/': `/${$('#lang').val()}`}" class="button link-to-cart">${translate('Перейти в каталог')}</a>
         </div>
         </div>
         <div class="loader-wrap login-loader" id="product_loader">
         <div class="loader-block">
         <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
         </div>
         </div>
         </div>
         </div>
      `

      $('body').css('overflow', 'hidden');
      $('div.body div.section').append(modalHTML);
   }

   stopPreloader(type){
      $('#modal_status_add_product').find('#product_loader').addClass('hide');
      if(type == 'error'){
         $('#modal_status_add_product').find('.form-message-success.error').removeClass('hide');
         
      }else if(type == 'ok'){
         $('#modal_status_add_product').find('.form-message-success.cool').removeClass('hide');
      }
   }
}