import * as $ from 'jquery';
import { initFormWithValidate } from "../form";
import { refreshForm } from '../helper/helpers';
import { translate } from '../helper/helpers';

export default class InitChooseForm{
   constructor(){
      this.$form = $('#modal_sizes');

      if(!this.$form.length) return false;

      this.successForm = this.successForm.bind(this);
      this.errorForm = this.errorForm.bind(this);
      this.clearForm = this.clearForm.bind(this);

      this.init();
   }

   init(){
      initFormWithValidate(this.$form);
      this.initFormHandler();
      this.submit(this.successForm, this.errorForm);
   }

   initFormHandler(){
      $('#modal_sizes').on('change','input.input-count',e=>{
         let valuesArray = $('#modal_sizes').serializeArray();

         valuesArray.pop();

         let isNoEmpty = valuesArray.some((item,index) => {
               return item.value !== '';
         })

         if(isNoEmpty){
            refreshForm();
         }else{
            $('#modal_sizes').find('button').addClass('inactive');
            $('.choose-cost').text(`${translate('Стоимость')}: 0 €`);
            $('#choose_count').text('0');
            $('#choose_sum').text('0 €');
         }
      })

      $('input.input-count').on('input', e=>{
         const $this = e.currentTarget;
         if($($this).val() !== ''){
            $($this).siblings('span.label').css('display', 'none');
            $($this).css('border','1px solid #41A0FF')
         }else{
            $($this).siblings('span.label').css('display', 'block');
            $($this).css('border','1px solid #ECECEC')
         }
      })
   }

   submit(successFn, errorFn) {
      this.$form.on('submit',  (e)=> {
         e.preventDefault();

         let $formData = [];

         $('#modal_sizes').find('input.input-count').each(function (index) {
            $formData[index] = {
               price:`${$(this).data('price')}`,
               color:`${$(this).data('color')}`,
               colorId:`${$(this).data('color-id')}`,
               count:`${$(this).val()}`,
               size:`${$(this).data('size')}`
            }
         });

         let items = [];

         items = $formData.filter( item => {
            return item.count != '';
         });

        /* let articleValue = $('#item_head_article').text().match(/\d/g).join('');*/
         let articleValue = $('#item_head_article').text().split(':')[1].slice(1);
         let commentaryValue = $('#choose_commentary').val();

         let ajaxUrl = this.$form.attr('action');
         let ajaxData = {
            userId:$('#user').val(),
            articleId : articleValue,
            items,
            commentary:commentaryValue
         };

         let headerItemsCount = items.reduce((sum,curr) => sum + +curr.count,0);

        /*  console.log('Данные формы:',ajaxData); */

         if($('#modal_sizes').hasClass('filled')){
            ajaxData = JSON.stringify(this.collectOrders());
            ajaxUrl = '/ajax/editCart/';
         }
         
         //модалка
         this.startPreloaderWithModal();

         $.ajax({
            url: ajaxUrl,
            type: 'POST',
            data: ajaxData,
            dataType: 'text',
            success:  (res) => {
               successFn(headerItemsCount)
            },
            error:  (res) => {
               errorFn(res);
            },
            timeout: 30000
         });
      })
   }

   successForm(headerItemsCount) {
      this.stopPreloader('ok');
      let currHeaderCount = +$('.header-ui__count').text();
      $('.header-ui__count').text(currHeaderCount + headerItemsCount);
   }

   errorForm(res) {
      this.stopPreloader('error');
   }

   collectOrders(){

      let data ={
         userId : $('#user').val(),
         products : {
            article:$('#item_head_article').text().match(/\d/g).join(''),
            commentary:$('#modal_sizes').find('input.input-text').val(),
            items:[]
         },
         orderId:$('.choose-block-wrapper').find('.field.numberic.filled input.input-count').data('order-id'),
         fullObject: 1
      };

      $('.choose-block-wrapper.open').each((index, order)=>{
         if(this.collectOrderItem(order).length !== 0){
            data.products.items = data.products.items.concat(this.collectOrderItem(order));
         }
      });

      return data;
   }

   collectOrderItem(order){

      let result = []

      $(order).find('input.input-count').each((i,item)=>{

         if($(item).data('deleted') == 'true'){
            return;
         }

         if($(item).val() == ''){
            return;
         }

         let simpleColorObj = {
            atributeId : $(item).data('atribute-id'),
            colorId : $(item).data('color-id'),
            size : $(item).data('size'),
            count : +$(item).val(),
            price : $(item).data('price'),
         }

         result.push(simpleColorObj);
      });

      return result;
   }

   clearForm() {
      this.$form[0].reset();
      this.$form.find('.field').removeClass('success').addClass('empty');
   }

   startPreloaderWithModal() {
      let modalHTML = `
         <div class="modal-after-product" id="modal_status_add_product">
         <div class="modal-content-status">
         <div class="form-message-success cool hide">
         <div class="form-info-success">
            <span class="body-16 close-status-modal">Закрыть</span>
            <div class="icon-block"><i class="icon icon-checked"></i></div>
            <h4 class="message-title">${translate('Отлично')}</h4>
            <p class="message-subtitle">${translate('Ваш товар добавлен в корзину')}</p>
            <div class="buttons-block">
               <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `/${$('#lang').val()}`}/cart" class="button link-to-cart">${translate('Перейти в корзину')}</a>
               <a href="${window.location.href}" class="button link-to-product">${translate('Продолжить покупки')}</a>
            </div>
         </div>
         </div>
         <div class="form-message-success error hide">
         <div class="form-info-success">
            <span class="body-16 close-status-modal">Закрыть</span>
            <div class="icon-block error"><i class="icon icon-close"></i></div>
            <h4 class="message-title">${translate('Ошибка')}</h4>
            <p class="message-subtitle">${translate('Попробуйте ещё раз')}</p>
            <div class="buttons-block">
               <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `/${$('#lang').val()}`}/cart" class="button link-to-cart">${translate('Перейти в корзину')}</a>
               <a href="${window.location.href}" class="button link-to-product">${translate('Продолжить покупки')}</a>
            </div>
         </div>
         </div>
         <div class="loader-wrap login-loader" id="product_loader">
         <div class="loader-block">
         <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
         </div>
         </div>
         </div>
         </div>
      `

      $('div[data-modal="choose-modal"] .modal-content').append(modalHTML);
      $('div[data-modal="choose-modal"]').css('overflow', 'hidden');
      $('.close-status-modal').on('click', ()=>{
         $('#modal_status_add_product').remove();
         $('div[data-modal="choose-modal"]').css('overflow', 'auto');
      })
   }

   stopPreloader(type){
      $('#modal_status_add_product').find('#product_loader').addClass('hide');
      if(type == 'error'){
         $('#modal_status_add_product').find('.form-message-success.error').removeClass('hide');
         
      }else if(type == 'ok'){
         $('#modal_status_add_product').find('.form-message-success.cool').removeClass('hide');
      }
   }
}