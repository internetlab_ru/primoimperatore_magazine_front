import * as $ from 'jquery';
import Swiper from 'swiper/bundle';
import {numberFormat} from '../helper';
import {getRightPrice, refreshForm, translate} from '../helper/helpers';
import {InitChooseForm} from '../init-choose-form';
import {initPlaceholders} from '../form';

export class Product {
   constructor() {
      this.$item = $('#slider_trigger');
      this.$itemWrapper = $('.item-block__content-item-info');
      this.$recomendations = $('#recomentadions_list');
      this.$table = $('#chose_item_table');
      this.$finalPriceTable = $('.final-price');

      this.$sliderInstance = '';

      if (!this.$item.length) return;

      this.isDesktop = window.innerWidth > 1000;
      this.init();
   }

   init() {
      this.getItem();
   }

   initSliderFunctions() {
      this.initModalItems();
      this.initSlider();
      this.initSlideTo();
   }

   initHandlers() {
      if ($('#small_gallery_desktop').length) {
         $('.item-block__content-gallery-item').on('click', e => {
            const $this = e.currentTarget;
            if (!$($this).hasClass('active')) {
               $('.item-block__content-gallery-item.active').removeClass('active');
               $($this).addClass('active');
               $('#slider_trigger').css('background-image', `${$($this).css('background-image')}`);
            }
         })
      }

      //скролл при клике
      $('body').on('click',"[data-scroll-trigger]",e =>{
         const isMobile = window.innerWidth < 500;
         $([document.documentElement, document.body]).animate({

            scrollTop: isMobile ? $("#chose_item_table").offset().top - 60 : $("#chose_item_table").offset().top
         }, 1000);
      })

      //отправка item
      $('body').on('click','#send_item_button',e => {
         if(!$('#send_item_button').hasClass('inactive')){
            $('body').css('overflow','hidden');
            this.sendProductToCart();
         }
      })

      //закрытие модалки
      $('body').on('click','.close-table-status',e =>{
         $('body').css('overflow','auto');
         $('#table_sending_status').addClass('hide');
      })

      //попап на цвет (новое)
      const $colorPopupBlock = $('.color-popup');

      $('body').on('mouseenter','.new-kind', e => {
         if(window.innerWidth < 1000) return false;
         const $colorBlock = $(e.currentTarget);

         const popupY = $colorBlock.position().top + 11;
         const popupX = $($colorBlock).position().left - $colorPopupBlock.width() / 4 + 2;
         const imageUrl = $colorBlock.data('image');
         const colorText = $colorBlock.data('color-name');

         $colorPopupBlock.find('.background').css('background',`url(${imageUrl})`)
         $colorPopupBlock.find('.text').text(colorText);
         $colorPopupBlock.css('top',popupY);

         $colorPopupBlock.css('left',popupX);
         $colorPopupBlock.removeClass('hide');

      });

      $('body').on('mouseleave','.new-kind', e => {
         $colorPopupBlock.addClass('hide');
      });

      $('body').on('click','#slider_trigger .swiper-button-next, #slider_trigger .swiper-button-prev', e => {
         e.stopPropagation()
      })

   }

   getItem() {
      const ajaxUrl = `/ajax/product/`;

      $.ajax({
         url: ajaxUrl,
         type: 'POST',
         dataType: 'JSON',
         data:{
            id : $("#message_id").data('product-id'),
            lang : $("#lang").val(),
            userId : $('#user').val()
         },
         success: (res) => {
            this.successFn(res);
            this.item = res.item;
         },
         error: (res) => {
             res = this.getMockup();
            this.successFn(res);
         },
         timeout: 30000
      });
   }

   successFn(res) {
      const resultHTML = this.renderItem(res.item);
      const recommendationsHTML = this.renderRecomendations(res.recomendations);
      const tableHTML = this.renderTableContent(res.item);


      this.$itemWrapper.append(resultHTML);
      this.$recomendations.append(recommendationsHTML);
      this.$table.append(tableHTML);

      if(res.cart.length !== 0){
         this.fillProductDataFromCart(res.cart);
      }

      //инстанс модальной формы выбора
      new InitChooseForm();

      initPlaceholders();

      $('.switcher').on('click', 'label', (e) => {
         const $this = e.currentTarget;
         const $parentBlock = $($this).parents('tr');
         $this.classList.toggle('active');

         if(!$($this).hasClass('active')){

            $parentBlock.find('input.input-count').each((index, input) => {
               $(input).val('');
               $(input).parents('.field.numberic').addClass('empty')
            })

            refreshForm();
         }
         return false;
      })

      $('.input-count').on('input',(e)=>{
         const $currentInput = $(e.currentTarget);
         const $parentRow = $currentInput.parents('.color-row');
         const $switcher =  $parentRow.find('.switcher-gray');

         if(!$switcher.hasClass('active')) {
            $switcher.addClass('active');
         }

         refreshForm()

      })
   }

   renderItem(item) {
      if (!item) return;
      const {
            name = '',
            article = '',
            minPrice = '',
            sizes = [],
            composition = '',
            collection = '',
            colors = [],
            description = '',
            photos = []
      } = item;

      let desktopGalleryHTML = '';
      let mobileGalleryHTML = '';

      photos.forEach((photo, index) => {
         desktopGalleryHTML += ` <div
            class="item-block__content-gallery-item${index === 0 ? ' active' : ''}"
            style="background-image: url(${photo})"
            data-img="${photo}"
            data-slide="${index}"
         ></div>`

         mobileGalleryHTML += `<div
            class="item-block__content-gallery-item "
            style="background-image: url(${photo})"
            data-img="${photo}"
            data-slide="${index}"
            data-open-modal-button="gallery-modal"
            data-effect-type="open-modal-fade-effect"
         ><div class="item-block__content-big-picture-icon hide-desktop"></div>
         </div>`
      })

      $('#small_gallery_desktop').append(desktopGalleryHTML);
      $('#small_gallery_mobile').append(mobileGalleryHTML);

      let sizesHTML = '';

      sizes.forEach(size => {
         sizesHTML += `<div class="size-button" data-scroll-trigger>
         <span class="size-button__size">${Object.keys(size)[0]}</span>
         <span class="size-button__price">${Object.values(size)[0]} €</span>
      </div>`
      })

      let colorsHTML = '';

      colors.forEach(color => {
            colorsHTML += `<div class="round-color new-kind" 
                data-image="${color.photo}" 
                data-color-name="${color.value}"
                style="background-image:url(${color.photo});" data-scroll-trigger></div>`
      })

      $('#item_head_collection_name_article').html(`<a href="/">${translate('Каталог')} ${collection.toLowerCase()}</a> / <a>${name} ${article}</a>`);
      $('#item_head_name').text(`${name}`);
      $('#item_head_article').text(`${translate('Артикул')}: ${article}`);
      $('#slider_trigger').css('border-radius','4px')

      let result = ` <div class="item-block__content-item-info__head">
      <h2 class="item-block__content-item-info__head-title">${translate('От')} ${numberFormat(minPrice,0,'',' ')} €</h2>
      <p>${translate('стоимость зависит от размера')}</p>
      <div class="item-block__content-item-info__head-sizes">
      ${sizesHTML}
      </div>
      </div>
      <div class="hr-dashed first"></div>
      <div class="item-block__content-item-info__item-parameters">
         <div class="item-compound">
            <span class="body-12">${translate('Состав')}</span>
            <p class="composition body-14">${composition}</p>
         </div>
         <div class="item-collection">
            <span class="body-12">${translate('Коллекция')}</span>
            <p class="collection body-14">${collection}</p>
         </div>
         <div class="item-colors">
            <span class="body-12">${translate('Доступные цвета')}</span>
            <div class="item-colors__colors-wrapper">
              ${colorsHTML}
            </div>
         </div>
      </div>
     
      <button class="button" id="open_choose_modal" data-scroll-trigger>${translate('Выбрать размер, цвет и количество')}</button>
   `

      /*после item-colors было описание:*/
      /* < div
      className = "item-description" >
          < span
      className = "body-12" >${translate('Описание')} < /span>
      <p className="description">${description}</p>
   </div>*/

      this.initHandlers();
      this.initSliderFunctions();

      return result;
   }

   renderRecomendations(recomendations) {
      if (!recomendations) return;
      let result = '';

      recomendations.forEach(recInfo => {
         result += `
             <div class="catalogue-card" data-image="${recInfo.photo}">
                  <a href="${recInfo.itemLink}">
                     <div class="upper-card-wrapper">
                     <div class="lozad same-things-card__image" data-src="${recInfo.photo}" alt=""
                        style="background-image:url(${recInfo.photo});" data-loaded="true"></div>
                     <p class="catalogue-card__name">${recInfo.name}</p>
                     <span class="catalogue-card__description">${translate('Артикул')}: ${recInfo.article} </span>
                     </div>
                     <div class="hr-and-price-wrapper">
                     <div class="hr-dashed"></div>
                     <div class="catalogue-card__price-block">
                        <span class="headline-5">от ${numberFormat(recInfo.price,0,'',' ')} €</span>
                        <i class="icon icon-arrow-down"></i>
                     </div>
                     </div>
                  </a>
               </div>
         `
      })

      return result;
   }

   renderTableContent(item) {
      if (!item) return;
      let result = '';
      let tableResult = '';

      let tableHeadHTML = `
        <thead>
            <tr>
             <th rowspan="2">Выбор цвета</th>
        `;

      let tableBodyHTML = `
           <tbody>
      `

      let sizesRow = '<tr>';

      //собираем thead
      item.sizes.forEach(size =>{
         const sizeCost = Object.values(size)[0];
         let trueSizeCost = +sizeCost;

         if(!Number.isInteger(trueSizeCost)){
            trueSizeCost = +sizeCost?.replace(',', '.');
         }

         tableHeadHTML += `<th class="size">${Object.keys(size)[0]}</th>`
         sizesRow += `<th class="size-cost">${getRightPrice(trueSizeCost)} €</th>`;
      });

      sizesRow += `</tr>`

      //собираем tbody
      item.colors.forEach(color => {
         tableBodyHTML += `
        <tr class="color-row" data-color-id="${color.id}">
            <td>
               <div class="first-color-cell"> 
                  <div class="color" style="background-image: url(${color.photo})"></div>
                  <div class="color-name">${color.value}</div>
                  <div class="switcher">
                     <label class="switcher-gray" data-color="${color.id}">
                        <input type="checkbox" data-toggle-view="">
                        <div class="bg-slide-item"></div>
                     </label>
                  </div>
               </div>
            </td>
         `


         item.sizes.forEach(size =>{
            const sizeCost = Object.values(size)[0];
            let trueSizeCost = +sizeCost;

            if(!Number.isInteger(trueSizeCost)){
               trueSizeCost = +sizeCost?.replace(',', '.');
            }

            tableBodyHTML += `
               <td class="input-cell">
                   <div class="field-count">
                        <label class="field numberic">
                           <input class="input-count item placeholder" maxlength="4" name="${Object.keys(size)[0]}_${color.value}"
                                  data-placeholder="0"  type="tel" data-color="${color.value}" data-atribute-id=""
                                  data-size="${Object.keys(size)[0]}" data-color-id="${color.id}" data-price="${trueSizeCost}">
                        </label>
                   </div>
               </td>`
         })

         tableBodyHTML += `
                  <td class="amount">0</td>
                  <td class="final">0 €</td>
               </tr>`
      })

      tableHeadHTML +=  `
            <th rowspan="2" class="head-amount">Кол-во</th>
            <th rowspan="2" class="head-final">Итого</th>
         </tr>`  + sizesRow + '</thead>'

      tableBodyHTML += '</tbody>';

      tableResult = tableHeadHTML + tableBodyHTML;


   const blockAfterTable = `<div class="modal-footer__wrapper">
      <h3>${translate('Итого')}</h3>
      <div class="modal-footer-block">
          <div class="text-block">
              <p>${translate('Товаров')}: <span id="choose_count">0</span></p>
              <p>${translate('Стоимость')}: <span id="choose_sum">0 €</span></p>
          </div>
            <label class="field field-input item email-label hide-tablet hide-desktop">
          <input class="input-text placeholder " name="choose_commentary"
             data-placeholder="${translate('Комментарий')}" type="text" id="choose_commentary">
      </label>
          <button id="send_item_button" type="submit" class="button inactive"><i class="icon icon-cart"></i>${translate('В корзину')}</button>
          </div>
           <label class="field field-input item email-label hide-only-mobile">
          <input class="input-text placeholder " name="choose_commentary"
             data-placeholder="${translate('Комментарий')}" type="text" id="choose_commentary">
      </label>
      </div>
   </div>`

   this.$finalPriceTable.append(blockAfterTable);

   this.fillModalMessage();

   return tableResult;
   }

   //заполнение модального окна выбора товара, если данный товар уже есть в корзине
   fillProductDataFromCart(cartItem){
      $('#chose_item_table').addClass('filled');
      cartItem.forEach(orderItem => {
         let filledWrapItem = $('#chose_item_table').find(`input[data-color-id=${orderItem.color}][data-size=${orderItem.size}]`);

         $(filledWrapItem).val(orderItem.count);
         $(filledWrapItem).attr('data-atribute-id',orderItem.atributeId);
         $(filledWrapItem).attr('data-order-id',orderItem.orderId);

         $(filledWrapItem).css('border',' 1px solid rgb(65, 160, 255)');

         $(filledWrapItem).parent('.field.numberic').addClass('filled');
         $(filledWrapItem).parents('.color-row').find('.switcher-gray').addClass('active');
      })

      if(cartItem[0].commentary !== ''){
         $('.final-price').find('input.input-text').addClass('filled');
         $('.final-price').find('input.input-text').val(cartItem[0].commentary);
      }
     
      refreshForm();
   }

   getMockup() {
      return {
         "item": {
            "name": "Футболка",
            "article": "194944",
            "minPrice": "1500",
            "photos": ['./img/test-photos/big-man.jpg', './img/test-photos/big-man2.jpg',
               './img/test-photos/big-man3.jpg', './img/test-photos/big-man.jpg', , './img/test-photos/big-man2.jpg', './img/test-photos/big-man3.jpg'
            ],
            "sizes": [{
               41: "1500,1"
            }, {
               42: 1800
            }, {
               43: 1300
            }, {
               44: 1800
            }, {
               45: 1800
            }, {
               46: 1400
            }, {
               47: 1800
            }, {
               48: 1800
            },  {
               59: 1000
            }, {
               60: 1800
            }],
            "composition": "Хлопок 97%, эластан 2%, полиэстер 1%",
            "collection": "Весна-лето 2021",
            "colors": [{
                  key: '#9fc8e0',
                  value: 'Обычная шерсть',
                  photo:'img/sherst1.jpg',
                  id:1
               },
               {
                  key: '#324563',
                  value: 'Классная шерсть',
                  photo:'img/sherst2.jpg',
                  id:2
               },
               {
                  key: '#fff',
                  value: 'Удивительная шерсть',
                  photo:'img/sherst3.jpg',
                  id:3
               }
            ],
            "description": "Новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка новая майка"
         },
         "recomendations": [
            {
               "name": "Футболка",
               "article": "194945",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1999",
            },
            {
               "name": "Футболка",
               "article": "194941",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1700",
            },
            {
               "name": "Футболка",
               "article": "194948",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1550",
            },
            {
               "name": "Футболка",
               "article": "194948",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1550",
            },
            {
               "name": "Футболка",
               "article": "194948",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1550",
            },
            {
               "name": "Футболка",
               "article": "194948",
               "photo": "img/test-photos/man.jpg",
               "itemLink": "/tshirt",
               "price": "1550",
            },
         ],
         "cart":[{"orderId":"187","atributeId":"452","article":"1412345123","commentary":"","size":"46","color":"1","count":"2"}]
      }
   }

   initSlider() {
      this.$sliderInstance = new Swiper('#gallery-modal__slider', {
         effect: 'slide',
         direction: 'horizontal',
         loop: false,
         preloadImages: false,
         lazy: true,
         slidesPerView: 1,
         resistance: false,
         allowTouchMove: true,
         spaceBetween: 0,
         navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
         }
      });

      new Swiper('#slider_trigger',{
         effect: 'slide',
         direction: 'horizontal',
         loop: false,
         preloadImages: false,
         lazy: true,
         slidesPerView: 1,
         resistance: false,
         allowTouchMove: true,
         spaceBetween: 0,
         navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
         },
         pagination: {
            el: ".swiper-pagination",
         },
      })
   };

   initModalItems() {
      let template = ``;

      if (this.isDesktop) {
         $('#small_gallery_desktop').find('.item-block__content-gallery-item').each((index, item) => {
            let img = item.dataset.img;
            template += this.getModalItemTemplate(img);
         });
      } else {
         $('#small_gallery_mobile').find('.item-block__content-gallery-item').each((index, item) => {
            let img = item.dataset.img;
            template += this.getModalItemTemplate(img);
         });
      }


      $('.swiper-wrapper').html(template);
   };

   getModalItemTemplate(image) {
      return `
         <div class="swiper-slide" style="background-image: url(${image})"></div>
      `;
   };

   initSlideTo() {
      $('.item-block__content-gallery').find('.item-block__content-gallery-item').on('click', e => {
         let slide = +$(e.currentTarget).attr('data-slide');
         this.$sliderInstance.slideTo(slide, 10, true);

         setTimeout(() => {
            this.$sliderInstance.update();
         }, 10);
      });
   };

   fillModalMessage(){
      const $modalBlock = $('#table_sending_status');

      const successMessage = `  <span class="body-16 close-status-modal close-table-status">Закрыть</span>
      <div class="icon-block"><i class="icon icon-checked"></i></div>
      <h4 class="message-title">${translate('Отлично')}</h4>
      <p class="message-subtitle">${translate('Ваш товар добавлен в корзину')}</p>
      <div class="buttons-block">
         <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `/${$('#lang').val()}`}/cart"
            class="button link-to-cart">${translate('Перейти в корзину')}</a>
         <a href="${window.location.href}" class="button link-to-product">${translate('Продолжить покупки')}</a>
      </div>`

      const errorMessage = `  <span class="body-16 close-status-modal close-table-status">Закрыть</span>
               <div class="icon-block error"><i class="icon icon-close"></i></div>
               <h4 class="message-title">${translate('Ошибка')}</h4>
               <p class="message-subtitle">${translate('Попробуйте ещё раз')}</p>
               <div class="buttons-block">
                  <a href="${$('#lang').val() == 'ru' || $('#lang').val() == '' ? '': `/${$('#lang').val()}`}/cart" class="button link-to-cart">${translate('Перейти в корзину')}</a>
                  <a href="${window.location.href}" class="button link-to-product">${translate('Продолжить покупки')}</a>
               </div>
      `

      $modalBlock.find('.form-message-success.cool .form-info-success').append(successMessage);
      $modalBlock.find('.form-message-success.error .form-info-success').append(errorMessage);
   }

   sendProductToCart(){
            let $formData = [];

            $('#chose_item_table').find('input.input-count').each(function (index) {
               $formData[index] = {
                  price:`${$(this).data('price')}`,
                  color:`${$(this).data('color')}`,
                  colorId:`${$(this).data('color-id')}`,
                  count:`${$(this).val()}`,
                  size:`${$(this).data('size')}`
               }
            });

            let items = [];

            items = $formData.filter( item => {
               return item.count != '';
            });
            let commentaryValue = $('#choose_commentary').val();

            let ajaxUrl = '/ajax/chosen/';
            let ajaxData = {
               userId:$('#user').val(),
               articleId:$("#message_id").data('product-id'),
               items,
               commentary:commentaryValue
            };

            let headerItemsCount = items.reduce((sum,curr) => sum + +curr.count,0);

            if($('#chose_item_table').hasClass('filled')){
               ajaxData = JSON.stringify(this.collectOrders());
               ajaxUrl = '/ajax/editCart/';
            }

            $('#table_sending_status').removeClass('hide');

            //обнуляем свичи
            $('.switcher-gray.active').each((_,switcher) => {
               $(switcher).trigger('click');
               refreshForm();
            })

            //обнуляем поле комментарий
            $('#choose_commentary').val('')

            $.ajax({
               url: ajaxUrl,
               type: 'POST',
               data: ajaxData,
               dataType: 'text',
               success:  (res) => {
                  this.successSendItem(headerItemsCount)
               },
               error:  (res) => {
                  this.errorSendItem(res);
               },
               timeout: 30000
            });
   }

   successSendItem(headerItemsCount) {
      $('#table_sending_status #product_loader').addClass('hide');
      $('#table_sending_status .form-message-success.cool').removeClass('hide')
      let currHeaderCount = +$('.header-ui__count').text();
      $('.header-ui__count').text(currHeaderCount + headerItemsCount);
   }

   errorSendItem(res) {
      $('#table_sending_status #product_loader').addClass('hide');
      $('#table_sending_status .form-message-success.error').removeClass('hide')
   }

   collectOrders(){

      let data ={
         userId : $('#user').val(),
         products : {
            article:$('#item_head_article').text().match(/\d/g).join(''),
            commentary:$('#choose_commentary').val(),
            items:[]
         },
         orderId:$('[data-order-id]').data('order-id'),
         fullObject: 1
      };

      $('#chose_item_table').find('.switcher-gray.active').each( (_,order) => {
         const $parentRowWithValues = $(order).parents('.color-row');

         data.products.items = data.products.items.concat(this.collectOrderItem($parentRowWithValues));
      });
      return data;
   }

   collectOrderItem(parentRow){
      let  result = [];

      $(parentRow).find('input.input-count').each((_,item)=>{
         if($(item).data('deleted') === 'true'){
            return;
         }

         if($(item).val() === ''){
            return;
         }

         result.push({
            atributeId: $(item).data('atribute-id'),
            colorId: $(item).data('color-id'),
            size: $(item).data('size'),
            count: +$(item).val(),
            price: $(item).data('price'),
         })
      });
      return result;
   }
}